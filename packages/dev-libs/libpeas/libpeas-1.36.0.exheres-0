# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ] gtk-icon-cache
require python [ blacklist=2 multibuild=false ] meson

SUMMARY="A GObject plugins library"
HOMEPAGE="http://live.gnome.org/Libpeas"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    gtk-doc
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gi-docgen[>=2021.7] )
    build+run:
        dev-libs/glib:2[>=2.44.0]
        gnome-bindings/pygobject:3[>=3.2.0][python_abis:*(-)?]
        gnome-desktop/gobject-introspection:1[>=1.39.0]
        x11-libs/gtk+:3[>=3.0.0][gobject-introspection]
"

RESTRICT="test" # requires X

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddemos=false
    -Dglade_catalog=false
    -Dintrospection=true
    -Dlua51=false
    -Dvapi=false
    -Dwidgetry=true

    -Dpython2=false
    -Dpython3=true
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
)

src_prepare() {
    meson_src_prepare

    # respect python abi
    edo sed \
        -e "s:'python3-embed':'python-$(python_get_abi)-embed':g" \
        -i meson.build
}

