# Copyright 2019-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require meson \
    vala [ vala_dep=true with_opt=true ]

SUMMARY="Library with GTK+ widgets for mobile phones"
DESCRIPTION="
libhandy provides GTK+ widgets and GObjects to ease developing applications for mobile phones.
"

LICENCES="LGPL-2.1"
MYOPTIONS="
    glade [[ description = [ build and install glade catalog ] ]]
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]
"

# require X access
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gtk-doc? ( dev-doc/gi-docgen[>=2021.1] )
    build+run:
        dev-libs/atk
        dev-libs/glib:2[>=2.44]
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.24.1][gobject-introspection?]
        x11-libs/pango
        glade? ( dev-util/glade )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dexamples=false
    -Dprofiling=false
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'glade glade_catalog'
    'gobject-introspection introspection'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    vapi
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

