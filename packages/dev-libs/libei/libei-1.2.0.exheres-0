# Copyright 2023 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix=https://gitlab.freedesktop.org user=libinput new_download_scheme=true ]
require meson

SUMMARY="library for Emulated Input"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: elogind systemd ) [[
        *description = [ Provider of the sd-bus library ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/libxml2:2.0
        dev-python/attrs
        dev-python/Jinja2
        x11-libs/libevdev
        x11-libs/libxkbcommon
        providers:elogind? ( sys-auth/elogind )
        providers:systemd? ( sys-apps/systemd )
    test:
        dev-python/pytest
        dev-python/pytest-xdist
"

MESON_SRC_CONFIGURE_OPTIONS=(
    'providers:elogind -Dsd-bus-provider=libelogind'
    'providers:systemd -Dsd-bus-provider=libsystemd'
)

# requires epoll-shim which isn't intended for linux
# as well as dev-python/structlog which is unwritten
RESTRICT="test"

