# Copyright 2012-2013 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Copyright 2023 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require python [ blacklist=2 multibuild=false ]
require gsettings gtk-icon-cache freedesktop-desktop freedesktop-mime meson
require test-dbus-daemon xdummy [ phase=test ]

SUMMARY="An IDE for writing GNOME-based software"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    sysprof [[ description = [ Support performance profiling via sysprof ] ]]
"

DEPENDENCIES="
    build:
        dev-doc/gtk-doc[>=1.32-r1]
        dev-libs/appstream
        dev-util/desktop-file-utils
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.22]
    build+run:
        app-spell/enchant:2 [[ note = [ src/plugins/spellcheck ] ]]
        app-text/cmark:=[>=0.29.0] [[ note = [ src/libide/gui ] ]]
        app-text/editorconfig [[ note = [ src/plugins/editorconfig ] ]]
        core/json-glib[>=1.2.0]
        dev-lang/clang:=[>=3.5] [[ note = [ src/plugins/clang ] ]]
        dev-libs/glib:2[>=2.75.0]
        dev-libs/icu:= [[ note = [ src/plugins/spellcheck ] ]]
        dev-libs/jsonrpc-glib:1.0[>=3.43.0]
        dev-libs/libadwaita:1[>=1.3.0][gobject-introspection]
        dev-libs/libdex:1[>=0.1.1][gobject-introspection]
        dev-libs/libpanel:1.0[>=1.1.2][gobject-introspection]
        dev-libs/libpeas:1.0[>=1.34.0]
        dev-libs/libxml2:2.0[>=2.9.0]
        dev-libs/pcre2 [[ note = [ src/libide/terminal ] ]]
        dev-libs/template-glib:1.0[>=3.36.1][gobject-introspection]
        dev-libs/vte:2.91[>=0.70.0][gobject-introspection][providers:gtk4]
        dev-scm/libgit2:=
        dev-scm/libgit2-glib:1.0[>=1.1.0]
        dev-util/universal-ctags [[ note = [ src/plugins/ctags ] ]]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
        gnome-desktop/gobject-introspection:1[>=1.75.0]
        gnome-desktop/gtksourceview:5[>=5.7.2][gobject-introspection]
        gnome-desktop/libsoup:3.0 [[ note = [ src/plugins/flatpak ] ]]
        net-libs/webkit:6.0[gobject-introspection]
        sys-apps/flatpak[>=1.10.2] [[ note = [ src/plugins/flatpak/daemon ] ]]
        sys-devel/libostree:1 [[ note = [ src/plugins/flatpak ] ]]
        sys-libs/libportal[providers:gtk4]
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk:4.0[>=4.8]
        x11-libs/pango
        sysprof? ( gnome-desktop/sysprof[>=3.46.0] )
    test:
        x11-libs/gtk:4.0[X]
    suggestion:
        app-virtualization/qemu
        dev-lang/go
        dev-lang/node
        dev-lang/python:*
        dev-lang/ruby:*
        dev-lang/rust:*[>=1.17]
        dev-lang/vala:*
        dev-python/Sphinx
        dev-util/valgrind
        sys-devel/cmake
        sys-devel/gdb
        sys-devel/gettext
        sys-devel/meson
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbuildtype=plain

    -Dchannel=other

    -Dhelp=false
    -Ddocs=false
    -Dnetwork_tests=false

    # libdeviced is not packaged
    -Dplugin_deviced=false
    # needs d-spy >= 1.4.0
    -Dplugin_dspy=false
    # podman is not packaged
    -Dplugin_podman=false
    # We don't provide any swift development tools
    -Dplugin_swift=false
    -Dplugin_swiftformat=false
    -Dplugin_swiftlint=false
    # update manager plugin is for flatpak-packaged builder self-updates
    -Dplugin_update_manager=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'sysprof plugin_sysprof'
)

src_prepare() {
    meson_src_prepare

    # 44.2: test-libide-io: Path substitution issues in the sandboxed build environment
    edo sed -e "/^test('test-libide-io'/d" -i src/tests/meson.build
}

src_test() {
    xdummy_start

    nonfatal test-dbus-daemon_run-tests meson_src_test
    declare ret=$?

    xdummy_stop
    [[ ${ret} != '0' ]] && die 'Tests failed'
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}
