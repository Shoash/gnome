# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome-bluetooth

SUMMARY="A fork of bluez-gnome focused on integration with the GNOME Desktop"
HOMEPAGE="https://live.gnome.org/GnomeBluetooth"

LICENCES="( GPL-2 LGPL-2.1 )"
SLOT="1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gtk-doc gobject-introspection
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxml2:2.0 [[ note = [ required for xmllint ] ]]
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        dev-libs/glib:2[>=2.44.0]
        media-libs/libcanberra[providers:gtk3]
        x11-libs/gtk+:3[>=3.12.0][gobject-introspection?]
        x11-libs/libnotify[>=0.7.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    run:
        group/plugdev [[ note = [ for rfkill udev rules ] ]]
        net-wireless/bluez[>=5.51]
        gnome-desktop/gnome-settings-daemon
    suggestion:
        gnome-desktop/gnome-bluetooth:3.0 [[ description = [ for bluetooth-sendto ] ]]
"

RESTRICT=test

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-build-Fix-build-for-newer-versions-of-meson.patch
)
MESON_SRC_CONFIGURE_PARAMS=(
    -Dicon_update=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    'gobject-introspection introspection'
)

src_install() {
    meson_src_install
    # bluetooth-sendto is provided by gnome-bluetooth:3.0
    edo rm "${IMAGE}"/usr/{share/{applications/bluetooth-sendto.desktop,man/man1/bluetooth-sendto.1},$(exhost --target)/bin/bluetooth-sendto}
    edo find "${IMAGE}" -type d -empty -delete
}
