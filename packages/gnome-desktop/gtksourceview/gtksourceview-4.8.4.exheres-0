# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix="tar.xz" ]
require vala [ vala_dep=true with_opt=true ]
require meson

SUMMARY="Text widget with syntax highlighting support"
HOMEPAGE="https://wiki.gnome.org/Projects/GtkSourceView"

LICENCES="LGPL-2.1"
SLOT="4.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    glade [[ description = [ Build and install glade catalog ] ]]
    gobject-introspection
    gtk-doc

    vapi [[ requires = gobject-introspection ]]
"

DEPENDENCIES="
    build:
        dev-util/itstool
        sys-devel/gettext[>=0.19.4]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.25] )
    build+run:
        dev-libs/fribidi[>=0.19.7]
        dev-libs/glib:2[>=2.48.0]
        dev-libs/libxml2:2.0[>=2.6.0]
        x11-libs/gtk+:3[>=3.24.0]
        glade? ( dev-util/glade[>=3.9] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.42.0] )
"

# requires X
RESTRICT="test"

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    'glade glade_catalog'
    'gobject-introspection gir'
    'vapi'
)

