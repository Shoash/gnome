# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache
require vala [ vala_dep=true ]
require meson

SUMMARY="Take photos and videos with your webcam, with fun graphical effects"
HOMEPAGE="https://wiki.gnome.org/Apps/Cheese"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
"

# needs X
RESTRICT="test"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.3
        dev-libs/libxslt [[ note = [ xsltproc ] ]]
        dev-util/itstool
        sys-devel/gettext[>=0.17]
        virtual/pkg-config[>=0.24]
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-libs/glib:2[>=2.38.0]
        gnome-desktop/gnome-desktop:4[legacy]
        media-libs/clutter-gst:3.0[>=3.0.0]
        media-libs/gstreamer:1.0[gobject-introspection?]
        media-libs/libcanberra[>=0.26][providers:gtk3]
        media-plugins/gnome-video-effects
        media-plugins/gst-plugins-bad:1.0[>=1.4]
        media-plugins/gst-plugins-base:1.0
        x11-libs/cairo
        x11-libs/clutter:1[>=1.13.2][gobject-introspection?]
        x11-libs/clutter-gtk:1.0
        x11-libs/cogl:1.0
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.13.4][gobject-introspection?]
        x11-libs/libX11
        x11-libs/libXtst
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.7] )
    run:
        media-plugins/gst-plugins-good:1.0[>=1.4][gstreamer_plugins:v4l]
"

MESON_SOURCE="${WORKBASE}/${PNV/_/.}"

# building and installing man is tied to gtk-doc magic, last checked: 3.34.0
MESON_SRC_CONFIGURE_PARAMS=(
    -Dman=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

