# Copyright 2013 Nathan Maxson <joyfulmantis@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'geocode-glib-0.99.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2007 Gentoo Foundation

require gnome.org [ suffix=tar.xz ] meson

SUMMARY="GLib geocoding library that uses the Yahoo! Place Finder service"
DESCRIPTION="
geocode-glib is a convenience library for the geocoding (finding longitude,
and latitude from an address) and reverse geocoding (finding an address from
coordinates). It uses Nominatim service to achieve that. It also caches
(reverse-)geocoding requests for faster results and to avoid unnecessary server
load.
"
HOMEPAGE="https://git.gnome.org/browse/geocode-glib/"

LICENCES="GPL-2"
SLOT="2.0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.6]
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.3] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.13] )
    build+run:
        core/json-glib[>=0.99.2][gobject-introspection?]
        dev-libs/glib:2[>=2.44]
        gnome-desktop/libsoup:3.0[>=2.99.0][gobject-introspection?]
       !gnome-desktop/geocode-glib:1.0[<3.26.4] [[
            description = [ File collisions due to common icons ]
            resolution = upgrade-blocked-before
        ]]
"

# Tests expect certain locales to be present
RESTRICT=test

MESON_SRC_CONFIGURE_PARAMS=(
    -Denable-installed-tests=false
    -Dsoup2=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    "gobject-introspection enable-introspection"
    "gtk-doc enable-gtk-doc"
)

