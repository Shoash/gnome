# Copyright 2023 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

GNOME_BACKGROUNDS_LATEST_SLOT=45

require alternatives

export_exlib_phases pkg_pretend src_install

SUMMARY="Backgrounds for the GNOME desktop"

SLOT=${SLOT:-$(ever major)}

LICENCES="GPL-2"
MYOPTIONS=""

if [[ ${SLOT} != "0" ]]; then
    DEPENDENCIES="
        build+run:
            !gnome-desktop/gnome-backgrounds:0[<43.1] [[
                description = [ Require gnome-backgrounds:0 version with alternatives support ]
                resolution = [ upgrade-blocked-before ]
            ]]
    "
fi

gnome-backgrounds_pkg_pretend() {
    if [[ ${SLOT} -gt ${GNOME_BACKGROUNDS_LATEST_SLOT} ]]; then
        eerror "When adding a new version of gnome-backgrounds, you must update"
        eerror "GNOME_BACKGROUNDS_LATEST_SLOT in gnome-backgrounds.exlib, and do a revision"
        eerror "bump on the package for the previous version"
        die "Additional work needed on gnome-backgrounds version bump"
    fi
}

gnome-backgrounds_src_install() {
    if [[ $(type -t meson_src_install) == 'function' ]]; then
        meson_src_install
    else
        default
    fi

    saved_shopt=$(shopt -p)
    shopt -s nullglob

    # Allow parallel-install of multiple gnome-backgrounds versions
    dodir /usr/share/backgrounds/gnome/${SLOT}
    dodir /usr/share/gnome-background-properties/${SLOT}

    declare metadata
    declare gbdir=/usr/share/backgrounds/gnome
    declare gbpdir=/usr/share/gnome-background-properties
    for metadata in "${IMAGE}/${gbpdir}"/*.xml; do
        metadata="${metadata#${IMAGE}}"
        declare name=${metadata%.xml}
        name=${name##*/}

        declare -a image_alternatives=()
        declare image image_basename
        for image in "${IMAGE}/${gbdir}/${name}"{-d,-l,}.*; do # nullglob used here
            image=${image#${IMAGE}}
            image_basename=${image##*/}
            image_alternatives+=( "${image}" "${SLOT}/${image_basename}" )
        done

        alternatives_for "_gnome-backgrounds_${name}" "${SLOT}" "${SLOT}" \
            "${gbpdir}/${name}.xml" "${SLOT}/${name}.xml" \
            "${image_alternatives[@]}"

        # The default wallpaper changes every release; make old versions available to select
        if [[ ${name} == 'adwaita' && ${SLOT} != '0' && ${SLOT} -lt ${GNOME_BACKGROUNDS_LATEST_SLOT} ]]; then
            edo cp "${IMAGE}/${gbpdir}/${SLOT}/${name}.xml" "${IMAGE}/${gbpdir}/${name}-${SLOT}.xml"
            edo sed -e "s#/gnome/#/gnome/${SLOT}/#g" -i "${IMAGE}/${gbpdir}/${name}-${SLOT}.xml"
        fi
    done

    eval "${saved_shopt}"
}

