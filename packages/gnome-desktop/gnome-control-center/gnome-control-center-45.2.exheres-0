# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gtk-icon-cache gsettings
require meson

SUMMARY="Configuration Applications for the GNOME Desktop"
HOMEPAGE="http://www.gnome.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    cups [[ description = [ Add support for managing printers ] ]]
    doc
    ibus [[ description = [ Add support for the IBus input method ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        doc? ( dev-libs/libxslt )
    build+run:
        dev-libs/atk
        dev-libs/glib:2[>=2.75.0]
        dev-libs/gnutls
        dev-libs/libadwaita:1[>=1.4.0]
        dev-libs/libepoxy[?X]
        dev-libs/libpwquality[>=1.2.2]
        dev-libs/libsecret:1
        dev-libs/libxml2:2.0
        gnome-desktop/colord-gtk[>=0.1.34][providers:gtk4]
        gnome-desktop/gcr:0
        gnome-desktop/gnome-bluetooth:3.0
        gnome-desktop/gnome-desktop:4
        gnome-desktop/gnome-online-accounts[>=3.25.3]
        gnome-desktop/gnome-settings-daemon:3.0[>=41.0]
        gnome-desktop/gsettings-desktop-schemas[>=42.0]
        gnome-desktop/gsound
        gnome-desktop/libgtop:2
        gnome-desktop/libgudev[>=232]
        gnome-desktop/tecla
        media-libs/fontconfig
        media-sound/pulseaudio[>=2.0]
        net-apps/NetworkManager[>=1.24.0]
        net-libs/libnma[>=1.10.2][providers:gtk4] [[
            note = [ required for network applet ]
        ]]
        net-wireless/ModemManager[>=0.7]
        sys-apps/accountsservice[>=0.6.39]
        sys-apps/colord[>=0.1.34]
        sys-apps/udisks:2[>=2.8.2]
        sys-apps/upower[>=0.99.8]
        sys-auth/polkit:1[>=0.103]
        x11-apps/setxkbmap
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0[>=2.23.0]
        x11-libs/gtk:4.0[>=4.11.2][X][wayland?]
        x11-libs/gtk+:3[X][wayland] [[ note = [ gnome-control-center-goa-helper ] ]]
        x11-libs/libX11[>=1.8]
        x11-libs/libXi[>=1.2]
        x11-libs/libwacom[>=0.7]
        x11-libs/pango
        cups? (
            net-fs/samba
            net-print/cups[>=1.4]
        )
        ibus? ( inputmethods/ibus[>=1.5.2] )
    run:
        gnome-desktop/libgnomekbd[>=2.91.91]
        cups? (
            app-admin/system-config-printer
            net-print/cups-pk-helper
        )
    recommendation:
        sys-apps/iio-sensor-proxy [[ description = [ Sensors integration in power panel ] ]]
        sys-apps/power-profiles-daemon [[ description = [ Power management in power panel ] ]]
        sys-apps/switcheroo-control [[ description = [ Information in the overview panel ] ]]
    test:
        dev-python/python-dbusmock
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/$(ever range 1)/* )

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dkerberos=false'
    '-Dprivileged_group=wheel'
    '-Dsnap=false'
    '-Dmalcontent=false'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'cups'
    'doc documentation'
    'ibus'
)

# Needs X access
RESTRICT="test"

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

